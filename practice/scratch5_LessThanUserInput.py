import random

a_list = []
app_running = True

"""Checks input """
def representsInt(n):
    try:
        int(n)
        return True
    except ValueError:
        return False

"""
1) Ask user to enter a numeric value
2) Generate a random list of 10 numbers
3) Display this list, then filter values that are LESS than user_request
4) Quit if user inputs Q or a non-numeric value
"""
while app_running:
    user_request = input("Enter a numeric value and compare against a random list of numbers (or Q to quit) \nValue:")

    if representsInt(user_request):
        a_list = random.sample(range(100), 10)
        print("New list:" + str(a_list) + "\nOf these values, the following are less than " + str(user_request))
        for x in a_list:
            if x < int(user_request):
                print(x, end=" ")

    elif user_request in ('q','Q'):
        app_running = False
        print("Quitting.")
        quit()

    else:
        print("No valid input provided. Please enter a whole number.")