"""Print all list items that are lower than 5"""

a_list = [1, 1.355, 14, 3, 5, 5.01, 6, 8, 9, 111, 34, 7, 88, 4.99]
new_list = []

"""all valid items printed in one line"""
print([a_item for a_item in a_list if a_item <5])

"""lists items one at a time"""
for a_item in a_list:
    if a_item <5:
        print(a_item)

"""creates a new list w/ only valid items"""
for a_item in a_list:
    if a_item <5:
        new_list.append(a_item)
        print(new_list)
