value = int(input("Give me a number and I'll tell you if it's even or odd: "))
num_check = value % 2

if num_check > 0:
    print("The number you entered is odd")
else:
    print("The number you entered is even")
