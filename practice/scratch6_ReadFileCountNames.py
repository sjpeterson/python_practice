"""Dict. list, to be populated w/ individual names"""
names_list = {}

"""Opens text file in same folder as script, strips each line of empty spaces, and adds line to dict."""
"""Otherwise, increment current dict item up one"""

with open('nameslist.txt') as names_file:
    line = names_file.readline()
    while line:
        line = line.strip()
        if line in names_list:
            names_list[line] += 1
        else:
            names_list[line] = 1
        line = names_file.readline()

print(names_list)